class ExperimentWRF():


    """
    Define an experimentWRF date intervals
    name
    last stage
    prefix to GFS
    subdomain

    """
    import config

    BOTTOMLAT = config.BOTTOMLAT
    TOPLAT = config.TOPLAT
    LEFTLON = config.LEFTLON
    RIGTHLON = config.RIGTHLON



    def __init__(self,
            start_date,
            end_date,
            logger,
            my_gfs,
            forcing_patern=None,
            template_namelit_input=None,
            template_namelist_wps=None,
            time_delta_input=10800,
            time_delta_output=60,
            geogrid = False ):

        from datetime import datetime, timedelta
        import datetime
        import config
        import os
        import sys
        import run_wrf

        assert start_date.hour in [0, 6, 12, 18]

        self.start_date = start_date
        self.end_date = end_date
        self.delta = (end_date - start_date)

        # in secods
        self.time_delta_input = time_delta_input
        # in minuts
        self.time_delta_output = time_delta_output

        i = datetime.datetime.now()
        self.name= i.isoformat()+"-"+start_date.strftime("%Y%m%d-%H")+"-"+\
        start_date.strftime("%Y%m%d-%H")

        self.name=start_date.strftime("%Y%m%d-%H")+"-"+\
                   end_date.strftime("%Y%m%d-%H") +"_"+\
                   str(i.year)+str(i.month).zfill(2)+str(i.day).zfill(2)+'.'+\
                   str(i.hour).zfill(2)+'.'+str(i.minute).zfill(2)+'.'+\
                   str(i.second).zfill(2)


        if (end_date - start_date) < timedelta(0):
            logger.error("end date is previous to start date")
            sys.exit()


        self.subdomains=config.SUBDOMAINS
        self.template_namelit_wps = 'namelist.wps.template'
        self.template_namelit_input = 'namelist.input.template'
        self.forcing_patern = forcing_patern
        self.path_results = config.PATH['experimets_results']+'/'+self.name

        if self.start_date < datetime.datetime(2015,1,1):
            self.vtable = config.VTABLE['gfs']
        else:
            self.vtable = config.VTABLE['gfs_2015']

        self.gfs = my_gfs
        self.geogrid = geogrid

        self.last_step = run_wrf.steps.keys()[0]
        self.debug = False


    def forecast_hours(self):

        delta = self.end_date-self.start_date

        return delta.days * 24 + delta.seconds // 3600



    def forcing_name(self, forecast_time):
        "forcing name gfs_yyyymmdd_anltime_forecastTime"

        return "gfs_%s_%s_%s.grib2"%\
        ( self.gfs.resolution(), self.start_date.strftime("%Y%m%d_%H"),
        str(forecast_time).zfill(3))

    def step(self, last_step):

        import pickle
        import run_wrf
        import config

        # print ": "+last_step
        assert last_step in run_wrf.steps.keys(), last_step
        self.last_step = last_step
        pickle.dump( self, open(config.PATH['tmp']+"/state.p", "wb"))




    def __str__(self):

        ret = "ExperimentWRF name: "+self.name+"\n\n"
        ret += 'Starting '+self.start_date.strftime("%Y/%m/%d %H:%M:%S")+'\n'
        ret += 'Ending '+self.end_date.strftime("%Y/%m/%d %H:%M:%S")+'\n'
        ret += "Compute forecast for "+str(self.delta.days)+" days "+\
        str(self.delta.seconds/3600)+":"+str(self.delta.seconds%60).zfill(2)+'\n'
        ret += 'last_step is: '+self.last_step+'\n'
        return ret
