# README #

A Backend for WRF (Wearther Research Forcasting)

The purpose of this project is make easily run WRF experiments run in operations mode
You can define easily an experiment which contains attributes like forecast interval, templates, etc.
A lot of information are storage in logins files.


* Encapsutation an experiment
* logging
* use templates (namelist.input and namelist.wps)

#How to install
Soon a pip package, but up now the hardly way:


1 Fork the repository and clone your copy
```
#!Bash

git clone https://USERNAME@bitbucket.org/USERNAME/run_wrf.git
```

2 Use virtualenv and change the python to 2.7.x if you have another version



Create the virtual environment for you application
```
#!Bash
virtualenv run_wrf
virtualenv -p /share/apps/python2.7/bin/python2.7 run_wrf
cd run_wrf
```

3 Get in virtual environment and install requirements
```
#!Bash
source bin/activate
pip install decorator python ipython-genutils Jinja2 MarkupSafe path.py pexpect pickleshare ptyprocess simplegeneric traitlets

```


4 Now you are ready for use it. But first you should edit config.py
and also edit the namelit.input and namelist.wps templates located in template folder or define
your own template and point it in config file.

For example if you want to run WRF for 27th of march of 2016 and forecast for 36 hs

```
#!Bash
./run_wrf.py --start_date 20160327-06 36 0.5

```
If you don't specify the date, take de last analisys time for example
if now is 2016 fri, 22th at 18:06, they run to  20160422-18 


if you need help you can use -h to display it

```
#!Bash
./run_wrf.py -h

```