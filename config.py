import os
import wrfLogger

DEBUG = False
LOGGER = None

CORES = 16
HOSTS = 1

TEMPLATE_WPS = 'namelist.wps.template'
TEMPLATE_INPUT = 'namelist.input.template'

BOTTOMLAT = "-50"
TOPLAT = "-05"
LEFTLON = "-85"
RIGTHLON = "-40"
SUBDOMAINS = 1

VTABLE = {}
VTABLE['gfs_2015'] = 'ungrib/Variable_Tables/Vtable.GFS_new_20150114'
VTABLE['gfs'] = 'ungrib/Variable_Tables/Vtable.GFS'

MPIEXEC = 'mpiexec'


# PATH['forcing']=PATH['work']+"/gfs

PATH = {}
PATH['work'] = "/home/pablohe/Build_WRF_Intel"
PATH['wps'] = PATH['work']+"/WPS"
PATH['wrf'] = PATH['work']+"/WRFV3/test/em_real"
PATH['experimets_results'] = PATH['work']+"/results"
PATH['scripts'] = os.getcwd()
PATH['tmp'] = '/tmp'
PATH['forcing'] = PATH['tmp']
PATH['geog_data_path'] = '/home/pablohe/Build_WRF_Intel/geog'

PROGRAMS = dict(
  link_grib = PATH['wps']+"/link_grib.csh",
  geogrid = PATH['wps']+"/geogrid.exe",
  metgrid = PATH['wps']+"/metgrid.exe",
  ungrib = PATH['wps']+"/ungrib.exe",
  real = PATH['wrf']+"/wps.exe",
  wrf = PATH['wrf']+"/wrf.exe",
)

PROGRAMS['curl'] = 'curl'

log_level = wrfLogger.LEVELS['debug']
