#!/usr/bin/env python

import config
import argparse
import ExperimentWRF
from datetime import datetime, timedelta
import wrfLogger
import logging
import utils

class Gfs(object):


    def __init__(self):
        self.experiment_wrf = None

    def check(self, out_file, ):

        import os.path
        import subprocess

        #file is complete
        command = ['wgrib2 '+out_file+' 2>&1 | grep "FATAL ERROR" | wc -l']
        file_uncompleted = int(subprocess.Popen(command, shell=True, stdout=subprocess.PIPE).stdout.read())
        # print command
        if file_uncompleted == 1:
            return False, 'integrity'


        ####################################################3


        # date in file is the analisys time
        start_date = self.experiment_wrf.start_date.strftime('%Y%m%d%H')
        command = ['wgrib2 '+out_file+' 2> /dev/null | head -n 1 | awk -F: \'{ print $3}\' | awk -F"=" \'{ print $2}\'']
        file_date = int(subprocess.Popen(command, shell=True, stdout=subprocess.PIPE).stdout.read())

        # print command
        if not int(start_date) == file_date:
            return False, 'date'

        ####################################################3
        # pdb.set_trace()
        # the hour is anl or be in the forecast hours
        command = ['wgrib2 '+out_file+' | head -n 1 | awk -F: \'{ print $6}\' | awk -F" " \'{ print $1}\'']
        tmp = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE).stdout.read()
        # print command
        if tmp == 'anl\n':
            forecast_time = 0
        else:
            forecast_time = int(tmp)


        if not (forecast_time in range(0, self.experiment_wrf.forecast_hours(), 3)):
            return False, 'hours'
        ####################################################3

        return True, 'File ok!'

class Gfs025(Gfs):


    def __init__(self):
        super(Gfs025, self).__init__()
        self.__resolution = 0.25

    def url(self, experiment_wrf, actual_date, actual_forecast):

        url = 'http://nomads.ncep.noaa.gov/cgi-bin/'

        file_ = 'filter_gfs_0p25.pl?file=gfs.t%sz.pgrb2.0p25.f%s'%\
        (experiment_wrf.start_date.strftime("%H"), str(actual_forecast).zfill(3))

        griburl = '&lev_0-0.1_m_below_ground=on'
        griburl += '&lev_0.1-0.4_m_below_ground=on'
        griburl += '&lev_0.4-1_m_below_ground=on'
        griburl += '&lev_1000_mb=on'
        griburl += '&lev_100_mb=on'
        griburl += '&lev_10_m_above_ground=on'
        griburl += '&lev_10_mb=on'
        griburl += '&lev_1-2_m_below_ground=on'
        griburl += '&lev_150_mb=on'
        griburl += '&lev_200_mb=on'
        griburl += '&lev_20_mb=on'
        griburl += '&lev_250_mb=on'
        griburl += '&lev_2_m_above_ground=on'
        griburl += '&lev_300_mb=on'
        griburl += '&lev_30_mb=on'
        griburl += '&lev_350_mb=on'
        griburl += '&lev_400_mb=on'
        griburl += '&lev_450_mb=on'
        griburl += '&lev_500_mb=on'
        griburl += '&lev_50_mb=on'
        griburl += '&lev_550_mb=on'
        griburl += '&lev_600_mb=on'
        griburl += '&lev_650_mb=on'
        griburl += '&lev_700_mb=on'
        griburl += '&lev_70_mb=on'
        griburl += '&lev_750_mb=on'
        griburl += '&lev_800_mb=on'
        griburl += '&lev_850_mb=on'
        griburl += '&lev_900_mb=on'
        griburl += '&lev_925_mb=on'
        griburl += '&lev_950_mb=on'
        griburl += '&lev_975_mb=on'
        griburl += '&lev_mean_sea_level=on'
        griburl += '&lev_surface=on'
        griburl += '&var_HGT=on'
        griburl += '&var_LAND=on'
        griburl += '&var_PRES=on'
        griburl += '&var_PRMSL=on'
        griburl += '&var_RH=on'
        griburl += '&var_SOILW=on'
        griburl += '&var_TSOIL=on'
        griburl += '&var_SPFH=on'
        griburl += '&var_TMP=on'
        griburl += '&var_UGRD=on'
        griburl += '&var_VGRD=on'
        griburl += '&var_WEASD=on'
        griburl += '&subregion='
        griburl += '&leftlon='+str(experiment_wrf.LEFTLON)
        griburl += '&rightlon='+str(experiment_wrf.RIGTHLON)
        griburl += '&toplat='+str(experiment_wrf.TOPLAT)
        griburl += '&bottomlat='+str(experiment_wrf.BOTTOMLAT)
        griburl += '&dir=%2Fgfs.'+experiment_wrf.start_date.strftime("%Y%m%d%H")

        return url, file_, griburl

    def resolution(self):
        return '0.25'

    def forcing_patern(self):
        return "gfs_0.25_????????_??_???.grib2"

class Gfs050(Gfs):
    """
    Define a 0.50 GFS class
    """


    def __init__(self):
        super(Gfs050, self).__init__()
        self._resolution = 0.5


    def resolution(self):
        return '0.50'


    def url(self, experiment_wrf, actual_date, actual_forecast):

        url = "http://nomads.ncep.noaa.gov/cgi-bin/"

        file_ = 'filter_gfs_0p50.pl?file=gfs.t%sz.pgrb2full.0p50.f%s'%\
        (experiment_wrf.start_date.strftime("%H"), str(actual_forecast).zfill(3))

        griburl = '&lev_0-0.1_m_below_ground=on'
        griburl += '&lev_0.1-0.4_m_below_ground=on'
        griburl += '&lev_0.4-1_m_below_ground=on'
        griburl += '&lev_1000_mb=on'
        griburl += '&lev_100_mb=on'
        griburl += '&lev_10_m_above_ground=on'
        griburl += '&lev_10_mb=on'
        griburl += '&lev_1-2_m_below_ground=on'
        griburl += '&lev_150_mb=on'
        griburl += '&lev_200_mb=on'
        griburl += '&lev_20_mb=on'
        griburl += '&lev_250_mb=on'
        griburl += '&lev_2_m_above_ground=on'
        griburl += '&lev_300_mb=on'
        griburl += '&lev_30_mb=on'
        griburl += '&lev_350_mb=on'
        griburl += '&lev_400_mb=on'
        griburl += '&lev_450_mb=on'
        griburl += '&lev_500_mb=on'
        griburl += '&lev_50_mb=on'
        griburl += '&lev_550_mb=on'
        griburl += '&lev_600_mb=on'
        griburl += '&lev_650_mb=on'
        griburl += '&lev_700_mb=on'
        griburl += '&lev_70_mb=on'
        griburl += '&lev_750_mb=on'
        griburl += '&lev_800_mb=on'
        griburl += '&lev_850_mb=on'
        griburl += '&lev_900_mb=on'
        griburl += '&lev_925_mb=on'
        griburl += '&lev_950_mb=on'
        griburl += '&lev_975_mb=on'
        griburl += '&lev_mean_sea_level=on'
        griburl += '&lev_surface=on'
        griburl += '&var_HGT=on'
        griburl += '&var_LAND=on'
        griburl += '&var_PRES=on'
        griburl += '&var_PRMSL=on'
        griburl += '&var_RH=on'
        griburl += '&var_SOILL=on'
        griburl += '&var_SOILW=on'
        griburl += '&var_TSOIL=on'
        griburl += '&var_SPFH=on'
        griburl += '&var_TMP=on'
        griburl += '&var_UGRD=on'
        griburl += '&var_VGRD=on'
        griburl += '&var_WEASD=on'
        griburl += '&subregion='
        griburl += '&leftlon='+str(experiment_wrf.LEFTLON)
        griburl += '&rightlon='+str(experiment_wrf.RIGTHLON)
        griburl += '&toplat='+str(experiment_wrf.TOPLAT)
        griburl += '&bottomlat='+str(experiment_wrf.BOTTOMLAT)
        griburl += '&dir=%2Fgfs.'+experiment_wrf.start_date.strftime("%Y%m%d%H")

        return url, file_, griburl



    def forcing_patern(self):
        return "gfs_0.50_????????_??_???.grib2"


def download(experiment_wrf):
    """
    get gfs forcing from ncep server ussing curl using experiment_wrf parameters
    (http://nomads.ncep.noaa.gov/cgi-bin/filter_gfs_0p50.pl?dir=%2Fgfs.2015011412)
    Supouse the experiment_wrf.start_date how analisys time.
    """

    import os

    actual_date = experiment_wrf.start_date

    forecast_hours = experiment_wrf.forecast_hours()
    actual_forecast = 0

    config.LOGGER.info('downloading GFS from nomads.ncep.noaa.gov ')

    while  actual_forecast <= forecast_hours:


        out_file = (config.PATH['tmp']+"/gfs_%s_%s_%s_%s.grib2")%(\
        experiment_wrf.gfs.resolution(),
        experiment_wrf.start_date.strftime("%Y%m%d"),\
        experiment_wrf.start_date.strftime("%H").zfill(2),\
        str(actual_forecast).zfill(3))

        url, file_, griburl = \
        experiment_wrf.gfs.url(experiment_wrf, actual_date, actual_forecast)

        if experiment_wrf.debug:
            print out_file
            experiment_wrf.gfs.check(out_file)
        else:
            if experiment_wrf.gfs.check(out_file)[0]:
                print 'allready donwload ' + out_file
                config.LOGGER.info('allready donwload '+out_file)
            else:
                utils.safe_run(os.system(config.PROGRAMS['curl']+\
                ' "'+url + file_ + griburl +'" -o '+ out_file), experiment_wrf)
                if experiment_wrf.gfs.check(out_file):
                    config.LOGGER.info(out_file+' Done!')


        actual_date += timedelta(hours=experiment_wrf.time_delta_input/3600)

        actual_forecast += experiment_wrf.time_delta_input/3600








def mkdate(datestr):
    return datetime.strptime(datestr, '%Y%m%d-%H')


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('start_date',type=mkdate)
    parser.add_argument('end_date',type=mkdate)
    parser.add_argument('resolution', type=float, choices=[0.5, 0.25], \
    help="0.5 or 0.25")

    parser.add_argument('--log_level', help="log_level could be: debug info \
    warning error or critical")
    parser.add_argument('--debug', action='store_true', default=False)

    args = parser.parse_args()

    if args.resolution == 0.25:
        my_gfs = Gfs_025()
    if args.resolution == 0.5:
        my_gfs = Gfs_050()



    my_experiment = ExperimentWRF.ExperimentWRF(args.start_date, args.end_date,\
    config.LOGGER, my_gfs)

    my_gfs.experiment_wrf = my_experiment

    config.LOGGER = wrfLogger.setup(logging.getLogger(),my_experiment)

    experiment_wrf.debug = args.debug



    download(my_experiment)
