#!/usr/bin/python

import config

class PostCondition():

    def check(self):
        pass



class PostConditionDownload(PostCondition):
    pass


class PostConditionUngrib(PostCondition):
    pass

class PostConditionGeogrid(PostCondition):
    pass

class PostConditionMetgrid(PostCondition):
    pass



class PostConditionReal(PostCondition):
    pass


class PostConditionWrf(PostCondition):
    pass
