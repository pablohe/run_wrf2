#!/usr/bin/python

import config

class PreCondition():

    def check(self):
        pass




class PreConditionDownload(PreCondition):

    def link_to_ftp(self):
        print 'checking ftp link'
        return True


    def files_ready(self):
        print 'files ready'
        return True


    def check(self):
        return self.link_to_ftp() and self.files_ready()






class PreConditionUngrib(PreCondition):
    pass

class PreConditionMetgrid(PreCondition):
    pass


class PreConditionGeogrid(PreCondition):
    pass

class PreConditionReal(PreCondition):
    pass


class PreConditionWrf(PreCondition):
    pass
