#!/usr/bin/env python
import run_wrf
import config
import wrfLogger
import logging


def main():
    """
    recover the las crashed run_wrf
    """
    import cPickle as pickle
    experiment_wrf = pickle.load( open( config.PATH['tmp']+"/state.p", "rb" ) )


    config.LOGGER = wrfLogger.setup(logging.getLogger(), \
    experiment_wrf, config.log_level)

    print experiment_wrf.last_step
    run_wrf.resume(experiment_wrf)


if __name__ == "__main__":

    main()
