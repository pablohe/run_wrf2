#!/usr/bin/env python

import argparse
from datetime import datetime, timedelta
# import pdb
import utils
import config
import logging
import ExperimentWRF
import wrfLogger
import gfs
import os
import inspect

import preCondition
import postCondition
from jinja2 import Environment, FileSystemLoader
from collections import OrderedDict

pre_condition_donwnload = preCondition.PreConditionDownload()

pre_condition_ungrib = preCondition.PreConditionUngrib()
pre_condition_geogrid = preCondition.PreConditionGeogrid()
pre_condition_metgrid = preCondition.PreConditionMetgrid()

pre_condition_real = preCondition.PreConditionReal()
pre_condition_wrf = preCondition.PreConditionWrf()


post_condition_donwnload = postCondition.PostConditionDownload()

post_condition_ungrib = postCondition.PostConditionUngrib()
post_condition_geogrid = postCondition.PostConditionGeogrid()
post_condition_metgrid = postCondition.PostConditionMetgrid()

post_condition_real = postCondition.PostConditionReal()
post_condition_wrf = postCondition.PostConditionWrf()



def clean_environment_wps(experiment_wrf):

    os.chdir(config.PATH['wps'])
    config.LOGGER.info("\ncleanning enviroment WPS")
    for elem in ['met_em*', 'GRIBFILE.???', 'FILE:*', 'PFILE*', '*.log', 'gfs_*']:
        utils.safe_run_subprocess("rm -f "+elem, experiment_wrf)



def clean_environment_wrf(experiment_wrf):

    os.chdir(config.PATH['wrf'])
    config.LOGGER.info("\ncleanning enviroment WRF")

    for elem in ['rsl*', 'nohup.out', 'wrfbdy_d01', 'wrfinput_d0*', 'met_em*']:
        utils.safe_run_subprocess("rm -f "+elem, experiment_wrf)



def prepare_forcing_for_preprocessing(experiment_wrf):

    "link forcing in working path"

    import glob

    os.chdir(config.PATH['wps'])

    for _file in glob.glob(experiment_wrf.gfs.forcing_patern()):
        os.remove(config.PATH['wps']+"/"+_file)

    for forecast_hour in \
        range(0, experiment_wrf.forecast_hours()+1, \
        experiment_wrf.time_delta_input/3600):

        forcing_name = experiment_wrf.forcing_name(forecast_hour)
        try:
            fp = open(config.PATH['forcing']+"/"+forcing_name)
        except IOError, e:
            config.LOGGER.error(str(e))

        command = "ln -sf "+config.PATH['forcing']+"/"\
        +forcing_name+' ./'

        config.LOGGER.info('preparing WRF forcing (GFS) ')
        utils.safe_run_subprocess(command, experiment_wrf)



def get_met(experiment_wrf):

    """
    link forcing to wps directory
    """

    os.chdir(config.PATH['wrf'])

    actual_date = experiment_wrf.start_date
    end_date = experiment_wrf.end_date


    while (end_date - actual_date ) >= timedelta(0):

        for subdomain in range(1,experiment_wrf.subdomains+1):

            file_name = "met_em.d"+str(subdomain).zfill(2)+\
            "."+actual_date.strftime("%Y-%m-%d_%H:%M:%S")+".nc"

            if os.path.isfile(config.PATH['wps']+"/"+file_name):

                command = "ln -sf "+\
                config.PATH['wps']+"/"+file_name+" "+\
                config.PATH['wrf']+"/"
                utils.safe_run_subprocess(command, experiment_wrf)

                config.LOGGER.info('linking met_em:'+command )
            else:
                config.LOGGER.error(config.PATH['wps']+"/"+file_name+" not exists")

        actual_date = actual_date+timedelta(hours=experiment_wrf.time_delta_input/3600)





def preprocessing(experiment_wrf):
    """
    Run preprocessing system: ungrib.exe and metgrid.exe
    geogrid.exe is optional
    """
    os.chdir(config.PATH['wps'])
    utils.safe_run_subprocess(config.PROGRAMS['link_grib']+" "+\
    experiment_wrf.gfs.forcing_patern(), experiment_wrf)

    command = "ln -sf %s ./Vtable "%experiment_wrf.vtable

    config.LOGGER.info('preparing WRF forcing (GFS) ')
    utils.safe_run_subprocess(command, experiment_wrf)



    if experiment_wrf.geogrid:
        pre_condition_geogrid.check()
        utils.safe_run_subprocess(config.PROGRAMS['geogrid'], experiment_wrf)
        post_condition_geogrid.check()

    pre_condition_ungrib.check()
    utils.safe_run_subprocess(config.PROGRAMS['ungrib'], experiment_wrf)
    post_condition_ungrib.check()

    pre_condition_metgrid.check()
    utils.safe_run_subprocess(config.PROGRAMS['metgrid'], experiment_wrf)
    post_condition_metgrid.check()



def  make_namelist_wrf(experiment_wrf):

    make_namelist(experiment_wrf, config.PATH['wrf']+"/namelist.input", \
    experiment_wrf.template_namelit_input)




def  make_namelist_wps(experiment_wrf):

    make_namelist(experiment_wrf, config.PATH['wps']+"/namelist.wps", \
    experiment_wrf.template_namelit_wps)




def  make_namelist(experiment_wrf, out_file, template):

    # from jinja2 import Environment, FileSystemLoader
    import os

    start = experiment_wrf.start_date
    end = experiment_wrf.end_date
    delta = end-start

    days = delta.days
    hours = delta.seconds/3600
    minuts = delta.seconds%3600/60
    seconds = delta.seconds - (hours*3600 + minuts * 60 )

    env = Environment(loader=FileSystemLoader(config.PATH['scripts']+'/templates'))

    template = env.get_template(template)

    template_vars = {"days"    : str(days).zfill(2),
                 "hours"   : str(hours).zfill(2),
                 "minuts"  : str(minuts).zfill(2),
                 "seconds" : str(seconds).zfill(2),
                 "start_year"  : str(start.year).zfill(2),
                 "start_month" : str(start.month).zfill(2),
                 "start_day"   : str(start.day).zfill(2),
                 "start_hour"  : str(start.hour).zfill(2),
                 "start_minute": str(start.minute).zfill(2),
                 "start_second": str(start.second).zfill(2),
                 "end_year"  : str(end.year).zfill(2),
                 "end_month" : str(end.month).zfill(2),
                 "end_day"   : str(end.day).zfill(2),
                 "end_hour"  : str(end.hour).zfill(2),
                 "end_minute": str(end.minute).zfill(2),
                 "end_second": str(end.second).zfill(2),
                 "geog_data_path":config.PATH['geog_data_path'],
                 "time_delta_output": str(experiment_wrf.time_delta_output),
                 "time_delta_input": str(experiment_wrf.time_delta_input),
              }


    output_from_parsed_template = template.render(template_vars)

    with open(out_file, "wb") as fh:
        fh.write(output_from_parsed_template)



def real(experiment_wrf):
    """ run real.exe """
    os.chdir(config.PATH['wrf'])

    pre_condition_real.check()
    utils.safe_run_subprocess("./real.exe", experiment_wrf)
    post_condition_wrf.check()



def wrf(experiment_wrf):
    """" run wrf.exe """
    os.chdir(config.PATH['wrf'])

    pre_condition_wrf.check()
    utils.safe_run_subprocess(\
    config.MPIEXEC+" -np "+str(config.CORES)+" ./wrf.exe", experiment_wrf)
    post_condition_wrf.check()



def  make_graphics(experiment_wrf):
    pass

def  move_graphics(experiment_wrf):
    pass

def move_output(experiment_wrf):
    """
    move results wrfout, namelist.wps, namelist.input
    """
    import shutil
    import glob

    if not experiment_wrf.debug:
        src = config.PATH['wrf']
        files = glob.iglob(os.path.join(src, "rsl.*"))
        dest = experiment_wrf.path_results

        for file_ in files:
            if os.path.isfile(file_):
                try:
                    shutil.copy(file_, dest)
                    config.LOGGER.info("move "+file_+" to "+dest)
                except IOError, e:
                    config.LOGGER.error("Unable to copy file. %s" % e)


        src = config.PATH['wrf']+"/namelist.input"
        dest = experiment_wrf.path_results
        try:
            shutil.move(src, dest)
            config.LOGGER.info("move "+src+" to "+dest)
        except IOError, e:
            config.LOGGER.error("Unable to copy file. %s" % e)

        src = config.PATH['wps']+"/namelist.wps"
        dest = experiment_wrf.path_results
        try:
            shutil.move(src, dest)
            config.LOGGER.info("move "+src+" to "+dest)
        except IOError, e:
            config.LOGGER.error("Unable to copy file. %s" % e)

        src = config.PATH['wrf']
        files = glob.iglob(os.path.join(src, "wrfout_d*"))
        dest = experiment_wrf.path_results

        for file_ in files:
            if os.path.isfile(file_):
                try:
                    shutil.move(file_, dest)
                    config.LOGGER.info("move "+file_+" to "+dest)
                except IOError, e:
                    config.LOGGER.error("Unable to copy file. %s" % e)


        experiment_wrf.last_step = None

def get_gfs( experiment_wrf):

    gfs.download(experiment_wrf)


def mkdate(datestr):
    return datetime.strptime(datestr, '%Y%m%d-%H')


# module constant

steps = OrderedDict()
steps['clean_environment_wps'] = clean_environment_wps
steps['get_gfs'] = get_gfs
steps['prepare_forcing_for_preprocessing'] = prepare_forcing_for_preprocessing
steps['make_namelist_wps'] = make_namelist_wps
steps['preprocessing'] = preprocessing
steps['clean_environment_wrf'] = clean_environment_wrf
steps['make_namelist_wrf'] = make_namelist_wrf
steps['get_met'] = get_met
steps['real'] = real
steps['wrf'] = wrf
steps['move_output'] = move_output



def forecast_hours(value):
    ivalue = int(value)
    assert ivalue % 3 == 0, "%s is an invalid forecast hour"%value
    return ivalue

def resume(experiment_wrf):

    print '---------------------------------------------'
    print experiment_wrf
    print '---------------------------------------------'
    print '\n\n'
    # import pdb; pdb.set_trace()
    flag = False
    for step in steps:
        if experiment_wrf.last_step == step:
            flag = True
        if flag:
            if experiment_wrf.debug:
                print '---------------------'+step+'---------------------'
            steps[step](experiment_wrf)
            experiment_wrf.step(step)
            if experiment_wrf.debug: print '\n\n'


def main():
    """
    Main function
    """

    parser = argparse.ArgumentParser()
    parser.add_argument('anl_time', type = forecast_hours,
    help = 'must be 0, 6, 12 or 18', choices = [0, 6, 12, 18])

    parser.add_argument('forecast_hours', type = forecast_hours,
    help = 'hour must be 3, 6, 9, 12, etc')

    parser.add_argument('resolution', type = float, choices = [0.5, 0.25], \
    help = "GFS resolution 0.5 or 0.25 degre")

    parser.add_argument('--start_date', type = mkdate, \
    help = 'must be 00, 06, 12 or 18 and this format YYYYMMDD-HH')

    parser.add_argument('--step', choices = steps.keys(), \
    help = 'run only this step')

    parser.add_argument('--log_level', help = "log_level could be: debug info \
    warning error or critical", default = wrfLogger.LEVELS['debug'])

    parser.add_argument('--debug', action = 'store_true', default=False)
    parser.add_argument('--geogrid', action = 'store_true', default=False)
    parser.add_argument('--resume', choices = steps.keys(), \
    help = "Step to continue included this choice")

    parser.add_argument('--recover', action='store_true', default=False)

    args = parser.parse_args()

    if not args.start_date:

        start_date = datetime(\
            datetime.today().year, \
            datetime.today().month, \
            datetime.today().day)
    else:
        start_date = args.start_date

    if args.resolution == 0.25:
        my_gfs = gfs.Gfs025()
    if args.resolution == 0.5:
        my_gfs = gfs.Gfs050()

    experiment_wrf = ExperimentWRF.ExperimentWRF(start_date,\
    start_date + timedelta(hours=args.forecast_hours),\
    config.LOGGER, my_gfs, )

    directory_results = config.PATH['experimets_results']+'/'+\
    experiment_wrf.name

    experiment_wrf.debug = args.debug


    if not os.path.exists(directory_results) and not experiment_wrf.debug:
        os.makedirs(directory_results)

    config.LOGGER = wrfLogger.setup(logging.getLogger(), \
    experiment_wrf, args.log_level)

    experiment_wrf.geogrid = args.geogrid

    experiment_wrf.gfs = my_gfs
    my_gfs.experiment_wrf = experiment_wrf

    if not (args.step or args.resume):
        for step in steps:
            steps[step](experiment_wrf)
            experiment_wrf.step(step)

    if args.step:
        steps.get(args.step)(experiment_wrf)
        experiment_wrf.step(args.step)

    if args.resume:
        experiment_wrf.last_step = args.resume
        resume(experiment_wrf)



    return

if __name__ == "__main__":

    main()
