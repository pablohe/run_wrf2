import unittest
import gfs
import config
import wrfLogger
import logging
from datetime import datetime, timedelta
import ExperimentWRF

class GFS(unittest.TestCase):


    # start_date = datetime(datetime.today().year,datetime.today().month, datetime.today().day)
    # end_date = datetime(datetime.today().year,datetime.today().month, datetime.today().day)+timedelta(days=1)

    # 0.25
    # ====
    # 0 a 6

    def __test_0_6(self, my_gfs):

        start_date = datetime(\
            datetime.today().year, \
            datetime.today().month, \
            datetime.today().day, \
            0)

        end_date = datetime(\
            datetime.today().year,\
            datetime.today().month,\
            datetime.today().day,
            6)

        start_date -= timedelta(days=1)
        end_date -= timedelta(days=1)


        experiment_wrf = ExperimentWRF.ExperimentWRF(\
            start_date, \
            end_date,\
            config.LOGGER,
            my_gfs,
            "gfs_??.grib2",
            "")

        config.LOGGER = wrfLogger.setup(logging.getLogger(), \
        experiment_wrf, wrfLogger.LEVELS['debug'])


        gfs.download(experiment_wrf)



    def __test_12_12(self, my_gfs):
    # 12 a 12 (+24hs)\
        start_date = datetime(\
            datetime.today().year, \
            datetime.today().month, \
            datetime.today().day, \
            12)

        end_date = datetime(\
            datetime.today().year,\
            datetime.today().month,\
            datetime.today().day,
            12)+timedelta(days=1)

        start_date -= timedelta(days=1)
        end_date -= timedelta(days=1)


        my_gfs = gfs.GFS_025()

        experiment_wrf = ExperimentWRF.ExperimentWRF(\
            start_date, \
            end_date,\
            config.LOGGER,
            my_gfs,
            "gfs_??.grib2",
            "")

        config.LOGGER = wrfLogger.setup(logging.getLogger(), \
        experiment_wrf, wrfLogger.LEVELS['debug'])


        gfs.download(experiment_wrf)

    def __test_18_12(self, my_gfs):
    # 18 a 12 (+18hs)
        start_date = datetime(\
            datetime.today().year, \
            datetime.today().month, \
            datetime.today().day, \
            18)

        end_date = datetime(\
            datetime.today().year,\
            datetime.today().month,\
            datetime.today().day,
            12)+timedelta(days=1)

        start_date -= timedelta(days = 1)
        end_date -= timedelta(days = 1)

        experiment_wrf = ExperimentWRF.ExperimentWRF(\
            start_date, \
            end_date,\
            config.LOGGER,
            my_gfs,
            "gfs_??.grib2",
            "")

        config.LOGGER = wrfLogger.setup(logging.getLogger(), \
        experiment_wrf, wrfLogger.LEVELS['debug'])


        gfs.download(experiment_wrf)

    def __test_0_0(self, my_gfs):
    # 0 a 0 (+24hs)


        start_date = datetime(\
            datetime.today().year, \
            datetime.today().month, \
            datetime.today().day, \
            0)

        end_date = datetime(\
            datetime.today().year,\
            datetime.today().month,\
            datetime.today().day,
            0)+timedelta(days = 1)
        start_date -= timedelta(days=1)
        end_date -= timedelta(days=1)


        experiment_wrf = ExperimentWRF.ExperimentWRF(\
            start_date, \
            end_date,\
            config.LOGGER,
            my_gfs,
            "gfs_??.grib2",
            "")

        config.LOGGER = wrfLogger.setup(logging.getLogger(), \
        experiment_wrf, wrfLogger.LEVELS['debug'])


        gfs.download(experiment_wrf)


    def test_0_6(self):

        my_gfs = gfs.GFS_025()
        self.__test_0_6(my_gfs)

        my_gfs = gfs.GFS_050()
        self.__test_0_6(my_gfs)


    def test_0_0(self):

        my_gfs = gfs.GFS_025()
        self.__test_0_0(my_gfs)

        my_gfs = gfs.GFS_050()
        self.__test_0_0(my_gfs)


    def test_12_12(self):

        my_gfs = gfs.GFS_025()
        self.__test_12_12(my_gfs)

        my_gfs = gfs.GFS_050()
        self.__test_12_12(my_gfs)

    def test_18_12(self):

        my_gfs = gfs.GFS_025()
        self.__test_18_12(my_gfs)

        my_gfs = gfs.GFS_050()
        self.__test_18_12(my_gfs)





    def integrity(self):
        """
        ok date
        ok hour
        no ok integrity
        """

        import shutil

        start_date = datetime(2016, 4, 19)
        end_date = datetime(2016, 4, 19, 3)

        my_gfs = gfs.GFS_050()

        shutil.copy(\
        './testing_data/gfs/gfs_0.50_20160419_00_000.grib2',\
        config.PATH['tmp']+'/gfs_0.50_20160419_00_000.grib2')

        shutil.copy(\
        './testing_data/gfs/gfs_0.50_20160419_00_003.grib2',\
        config.PATH['tmp']+'/gfs_0.50_20160419_00_003.grib2')

        experiment_wrf = ExperimentWRF.ExperimentWRF(\
            start_date, \
            end_date,\
            config.LOGGER,
            my_gfs,
            "gfs_??.grib2",
            "")

        config.LOGGER = wrfLogger.setup(logging.getLogger(), \
        experiment_wrf, wrfLogger.LEVELS['debug'])

        my_gfs.experiment_wrf = experiment_wrf

        actual_date = start_date

        forecast_hours = experiment_wrf.forecast_hours()
        actual_forecast = 0

        while  actual_forecast <= forecast_hours:

            out_file = (config.PATH['tmp']+"/gfs_%s_%s_%s_%s.grib2")%(\
            experiment_wrf.gfs.resolution(),
            experiment_wrf.start_date.strftime("%Y%m%d"),\
            experiment_wrf.start_date.strftime("%H").zfill(2),\
            str(actual_forecast).zfill(3))

            self.assertEqual(False, my_gfs.check(out_file)[0])
            self.assertEqual('integrity', my_gfs.check(out_file)[1])
            actual_date += timedelta(hours=experiment_wrf.time_delta_input/3600)
            actual_forecast += experiment_wrf.time_delta_input/3600


    def date(self):
        """"
        wrong date
        ok integrity
        """

        import shutil

        start_date = datetime(2016, 4, 18)
        end_date = datetime(2016, 4, 18, 03)

        my_gfs = gfs.GFS_050()

        shutil.copy(\
        './testing_data/gfs/gfs_0.50_20160418_00_000.grib2',\
        config.PATH['tmp']+'/gfs_0.50_20160418_00_000.grib2')

        shutil.copy(\
        './testing_data/gfs/gfs_0.50_20160418_00_003.grib2',\
        config.PATH['tmp']+'/gfs_0.50_20160418_00_003.grib2')

        experiment_wrf = ExperimentWRF.ExperimentWRF(\
            start_date, \
            end_date,\
            config.LOGGER,
            my_gfs,
            "gfs_??.grib2",
            "")

        config.LOGGER = wrfLogger.setup(logging.getLogger(), \
        experiment_wrf, wrfLogger.LEVELS['debug'])

        my_gfs.experiment_wrf = experiment_wrf

        actual_date = start_date

        forecast_hours = experiment_wrf.forecast_hours()
        actual_forecast = 0

        while  actual_forecast <= forecast_hours:

            out_file = (config.PATH['tmp']+"/gfs_%s_%s_%s_%s.grib2")%(\
            experiment_wrf.gfs.resolution(),
            experiment_wrf.start_date.strftime("%Y%m%d"),\
            experiment_wrf.start_date.strftime("%H").zfill(2),\
            str(actual_forecast).zfill(3))

            self.assertEqual(False, my_gfs.check(out_file)[0])
            self.assertEqual('date', my_gfs.check(out_file)[1])

            actual_date += timedelta(hours=experiment_wrf.time_delta_input/3600)
            actual_forecast += experiment_wrf.time_delta_input/3600

    def hours(self):
        """"
        # ok date
        # wrong hours
        # ok integrity

        """

        import shutil

        start_date = datetime(2016, 4, 19, 12)
        end_date = datetime(2016, 4, 19, 15)

        my_gfs = gfs.GFS_050()

        shutil.copy(\
        './testing_data/gfs/gfs_0.50_20160419_12_006.grib2',\
        config.PATH['tmp']+'/gfs_0.50_20160419_12_000.grib2')

        shutil.copy(\
        './testing_data/gfs/gfs_0.50_20160419_12_006.grib2',\
        config.PATH['tmp']+'/gfs_0.50_20160419_12_003.grib2')

        experiment_wrf = ExperimentWRF.ExperimentWRF(\
            start_date, \
            end_date,\
            config.LOGGER,
            my_gfs,
            "gfs_??.grib2",
            "")

        config.LOGGER = wrfLogger.setup(logging.getLogger(), \
        experiment_wrf, wrfLogger.LEVELS['debug'])


        my_gfs.experiment_wrf = experiment_wrf

        actual_date = start_date

        forecast_hours = experiment_wrf.forecast_hours()
        actual_forecast = 0

        while  actual_forecast <= forecast_hours:

            out_file = (config.PATH['tmp']+"/gfs_%s_%s_%s_%s.grib2")%(\
            experiment_wrf.gfs.resolution(),
            experiment_wrf.start_date.strftime("%Y%m%d"),\
            experiment_wrf.start_date.strftime("%H").zfill(2),\
            str(actual_forecast).zfill(3))

            print out_file
            self.assertEqual(False, my_gfs.check(out_file)[0])
            self.assertEqual('hours', my_gfs.check(out_file)[1])

            actual_date += timedelta(hours=experiment_wrf.time_delta_input/3600)
            actual_forecast += experiment_wrf.time_delta_input/3600


#  def test_isupper(self):
#      self.assertTrue('FOO'.isupper())
#      self.assertFalse('Foo'.isupper())

#  def test_split(self):
#      s = 'hello world'
#      self.assertEqual(s.split(), ['hello', 'world'])
#      # check that s.split fails when the separator is not a string
#      with self.assertRaises(TypeError):
#          s.split(2)

if __name__ == '__main__':
    unittest.main()
