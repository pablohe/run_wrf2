#!/bin/bash

# 14ene2015: -cyn-
# descargo las salidas con las nuevas actualizaciones
# agrego un log
# agrego un par de controles
# 16ene2015:
# agrego la variable TSOIL


#ESTE SCRIPT USA EL PROGRAMA CURL Y LAS HERRAMIENTAS DE LOS SERVIDORES DEL NCEP PARA BAJAR EFICIENTEMENTE UN SECTOR DE LA CORRIDA DEL GFS EN ALTA RESOLUCION PARA ANIDAR EL WRF.
export http_proxy="http://10.1.5.66:8080"

CICLO="00"
BOTTOMLAT="-50"
TOPLAT="-05"
LEFTLON="-85"
RIGTHLON="-40"
TODAY=`date +%Y%m%d`
#TODAY=20150903
CURL="curl"
GRIBPATH="/tmp/"

LOG=${GRIBPATH}'log.gfs.nuevo.'${CICLO}
rm ${LOG}
date >> ${LOG}  2>&1

cd ${GRIBPATH}

PLAZO_INI=00
PLAZO_FIN=48
INTERVALO=03

PLAZO=$PLAZO_INI

echo $TODAY $GRIBPATH $PLAZO_INI $PLAZO_FIN $PLAZO $CICLO

echo " --------------------------"  >> ${LOG} 2>&1


while [ ${PLAZO} -le ${PLAZO_FIN} ]
  do

  FILE="gfs_${PLAZO}.grib2"  >> ${LOG} 2>&1

  if [ -f ${GRIBPATH}${FILE} ]
    then
    echo ${FILE} "ARCHIVO YA BAJADO" >> ${LOG} 2>&1
    #si alguna vez lo descargo pero no verifica tamanio.
            SIZE=$( stat -c%s $FILE )
    if [ $SIZE -lt 1700000 ]
    then
      echo "  El archivo ${FILE} es mas chico de lo que debe ( 1.7Mb ). Lo descargamos." >> ${LOG} 2>&1

      # DIRECTORIO VIEJO    (http://nomads.ncep.noaa.gov/cgi-bin/filter_gfs_hd.pl?dir=%2Fgfs.2015011406%2Fmaster)
      # saque de despues de $CURL  --proxy $PROXY
      #$CURL "http://nomads.ncep.noaa.gov/cgi-bin/filter_gfs_hd.pl?file=gfs.t${CICLO}z.mastergrb2f${PLAZO}&lev_0-0.1_m_below_ground=on&lev_0.1-0.4_m_below_ground=on&lev_0.4-1_m_below_ground=on&lev_1000_mb=on&lev_100_mb=on&lev_10_m_above_ground=on&lev_10_mb=on&lev_1-2_m_below_ground=on&lev_150_mb=on&lev_200_mb=on&lev_20_mb=on&lev_250_mb=on&lev_2_m_above_ground=on&lev_300_mb=on&lev_30_mb=on&lev_350_mb=on&lev_400_mb=on&lev_450_mb=on&lev_500_mb=on&lev_50_mb=on&lev_550_mb=on&lev_600_mb=on&lev_650_mb=on&lev_700_mb=on&lev_70_mb=on&lev_750_mb=on&lev_800_mb=on&lev_850_mb=on&lev_900_mb=on&lev_925_mb=on&lev_950_mb=on&lev_975_mb=on&lev_mean_sea_level=on&lev_surface=on&var_HGT=on&var_LAND=on&var_PRES=on&var_PRMSL=on&var_RH=on&var_SOILL=on&var_SOILW=on&var_SPFH=on&var_TMP=on&var_UGRD=on&var_VGRD=on&var_WEASD=on&subregion=&leftlon=${LEFTLON}&rightlon=${RIGTHLON}&toplat=${TOPLAT}&bottomlat=${BOTTOMLAT}&dir=%2Fgfs.${TODAY}${CICLO}%2Fmaster" -o ${GRIBPATH}/gfs.t${CICLO}z.mastergrb2f${PLAZO}.grib2 >> ${LOG}  2>&1

      # DIRECTORIO NUEVO:   (http://nomads.ncep.noaa.gov/cgi-bin/filter_gfs_0p50.pl?dir=%2Fgfs.2015011412)
      # formato nuevo de nombre: 1800038
      $CURL "http://nomads.ncep.noaa.gov/cgi-bin/filter_gfs_0p50.pl?file=gfs.t${CICLO}z.pgrb2full.0p50.f0${PLAZO}&lev_0-0.1_m_below_ground=on&lev_0.1-0.4_m_below_ground=on&lev_0.4-1_m_below_ground=on&lev_1000_mb=on&lev_100_mb=on&lev_10_m_above_ground=on&lev_10_mb=on&lev_1-2_m_below_ground=on&lev_150_mb=on&lev_200_mb=on&lev_20_mb=on&lev_250_mb=on&lev_2_m_above_ground=on&lev_300_mb=on&lev_30_mb=on&lev_350_mb=on&lev_400_mb=on&lev_450_mb=on&lev_500_mb=on&lev_50_mb=on&lev_550_mb=on&lev_600_mb=on&lev_650_mb=on&lev_700_mb=on&lev_70_mb=on&lev_750_mb=on&lev_800_mb=on&lev_850_mb=on&lev_900_mb=on&lev_925_mb=on&lev_950_mb=on&lev_975_mb=on&lev_mean_sea_level=on&lev_surface=on&var_HGT=on&var_LAND=on&var_PRES=on&var_PRMSL=on&var_RH=on&var_SOILL=on&var_SOILW=on&var_TSOIL=on&var_SPFH=on&var_TMP=on&var_UGRD=on&var_VGRD=on&var_WEASD=on&subregion=&leftlon=${LEFTLON}&rightlon=${RIGTHLON}&toplat=${TOPLAT}&bottomlat=${BOTTOMLAT}&dir=%2Fgfs.${TODAY}${CICLO}" -o ${GRIBPATH}/gfs_${PLAZO}.grib2 >> ${LOG}  2>&1
    fi

  else
    echo ${FILE} "hay que descargarlo" >> ${LOG} 2>&1

    $CURL "http://nomads.ncep.noaa.gov/cgi-bin/filter_gfs_0p50.pl?file=gfs.t${CICLO}z.pgrb2full.0p50.f0${PLAZO}&lev_0-0.1_m_below_ground=on&lev_0.1-0.4_m_below_ground=on&lev_0.4-1_m_below_ground=on&lev_1000_mb=on&lev_100_mb=on&lev_10_m_above_ground=on&lev_10_mb=on&lev_1-2_m_below_ground=on&lev_150_mb=on&lev_200_mb=on&lev_20_mb=on&lev_250_mb=on&lev_2_m_above_ground=on&lev_300_mb=on&lev_30_mb=on&lev_350_mb=on&lev_400_mb=on&lev_450_mb=on&lev_500_mb=on&lev_50_mb=on&lev_550_mb=on&lev_600_mb=on&lev_650_mb=on&lev_700_mb=on&lev_70_mb=on&lev_750_mb=on&lev_800_mb=on&lev_850_mb=on&lev_900_mb=on&lev_925_mb=on&lev_950_mb=on&lev_975_mb=on&lev_mean_sea_level=on&lev_surface=on&var_HGT=on&var_LAND=on&var_PRES=on&var_PRMSL=on&var_RH=on&var_SOILL=on&var_SOILW=on&var_TSOIL=on&var_SPFH=on&var_TMP=on&var_UGRD=on&var_VGRD=on&var_WEASD=on&subregion=&leftlon=${LEFTLON}&rightlon=${RIGTHLON}&toplat=${TOPLAT}&bottomlat=${BOTTOMLAT}&dir=%2Fgfs.${TODAY}${CICLO}" -o ${GRIBPATH}/gfs_${PLAZO}.grib2 >> ${LOG}  2>&1

  fi

  echo ¨acaaaaa¨
  echo $CURL "http://nomads.ncep.noaa.gov/cgi-bin/filter_gfs_0p50.pl?file=gfs.t${CICLO}z.pgrb2full.0p50.f0${PLAZO}&lev_0-0.1_m_below_ground=on&lev_0.1-0.4_m_below_ground=on&lev_0.4-1_m_below_ground=on&lev_1000_mb=on&lev_100_mb=on&lev_10_m_above_ground=on&lev_10_mb=on&lev_1-2_m_below_ground=on&lev_150_mb=on&lev_200_mb=on&lev_20_mb=on&lev_250_mb=on&lev_2_m_above_ground=on&lev_300_mb=on&lev_30_mb=on&lev_350_mb=on&lev_400_mb=on&lev_450_mb=on&lev_500_mb=on&lev_50_mb=on&lev_550_mb=on&lev_600_mb=on&lev_650_mb=on&lev_700_mb=on&lev_70_mb=on&lev_750_mb=on&lev_800_mb=on&lev_850_mb=on&lev_900_mb=on&lev_925_mb=on&lev_950_mb=on&lev_975_mb=on&lev_mean_sea_level=on&lev_surface=on&var_HGT=on&var_LAND=on&var_PRES=on&var_PRMSL=on&var_RH=on&var_SOILL=on&var_SOILW=on&var_TSOIL=on&var_SPFH=on&var_TMP=on&var_UGRD=on&var_VGRD=on&var_WEASD=on&subregion=&leftlon=${LEFTLON}&rightlon=${RIGTHLON}&toplat=${TOPLAT}&bottomlat=${BOTTOMLAT}&dir=%2Fgfs.${TODAY}${CICLO}" -o ${GRIBPATH}/gfs_${PLAZO}.grib2

  PLAZO=`expr ${PLAZO} + ${INTERVALO}`

  if [ ${PLAZO} -lt 10  ]
  then
    PLAZO='0'${PLAZO}
  fi

done

echo " -------------FIN--------------" >> ${LOG}  2>&1
date >> ${LOG}  2>&1
